package com.slalla.ml.adaboost;

import com.slalla.ml.dataTransformation.DataHelper;
import com.slalla.ml.dataTransformation.FileHelper;
import com.slalla.ml.id3.*;

import java.io.FileNotFoundException;
import java.util.*;

import static com.slalla.ml.id3.ID3.getEntropy;

public class AdaBoost {

    public static void main(String[] args){
        //Select file to parse
        String[] fileNames = {"breast-cancer-wisconsin2.data", "car.data", "ecoli2.data", "letter-recognition.data", "mushroom2.data"};
        Scanner inputScanner = new Scanner(System.in);

        int input = 99;
        do {
            System.out.println("Welcome to the AdaBoost Classifier. Please select a dataset to work with:");
            for (int i = 0; i < fileNames.length; i++) {
                System.out.println(i + 1 + ". " + fileNames[i]);
            }
            input = inputScanner.nextInt();
        } while (input < 1 || input > fileNames.length);

        String fileName = fileNames[input - 1];

        int[] rowCols;
        try {
            rowCols = FileHelper.getNumRowsColumns(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        int numRows = rowCols[0];
        int numCols = rowCols[1];


        String[][] inputMatrix;

        try {
            inputMatrix = FileHelper.readInValues(numRows, numCols, fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }


        int classifierColumn = numCols;
        do {
            System.out.println("Please select the column you wish to predict. There are " + numCols + " columns.");
            classifierColumn = inputScanner.nextInt();
        } while (classifierColumn < 0 || classifierColumn >= numCols);

        int numTrees = 0;
        do {
            System.out.println("Please select the number of trees you wish to use in the AdaBoost algorithm.");
            numTrees = inputScanner.nextInt();
        } while (numTrees <= 0);


        //Number of times to run k-fold-cross validation
        ArrayList<Double> nTimeAccuracies = new ArrayList<>();
        for (int ntime = 0; ntime < 10; ntime++) {
            //Shuffle
            //inputMatrix = DataHelper.shuffleData(ntime, inputMatrix); used for predictable shuffled data
            inputMatrix = DataHelper.shuffleData(inputMatrix);

            int numberOfFolds = 5;
            ArrayList<String[][]> folds = DataHelper.kfoldSplit(inputMatrix, numberOfFolds);
            ArrayList<Double> kFoldAccuracies = new ArrayList<Double>();
            for (int i = 0; i < folds.size(); i++) {
                HashSet<Integer> foldsToUse = new HashSet<Integer>();
                int fold = i;
                while (foldsToUse.size() != numberOfFolds - 1) {
                    fold = (fold + 1) % (numberOfFolds);
                    foldsToUse.add(fold);
                }
                System.out.println(foldsToUse);
                String[][] combinedFold = DataHelper.combineFolds(folds, foldsToUse);
                String[][] testingFold = folds.get((fold + 1) % (numberOfFolds));


                HashSet<Integer> attributes = new HashSet<Integer>();
                for(int j = 0; j< numCols; j++){
                    if(j != classifierColumn){
                        attributes.add(j);
                    }
                }

                ArrayList<String[]> inputMatrixConvert = new ArrayList<String[]>(Arrays.asList(combinedFold));

                AdaBoostClassifier adaBoostClassifier = trainAdaBoostClassifier(inputMatrixConvert, classifierColumn, attributes, numTrees);

                int numTestExamples = testingFold.length;

                int correct = classifyMultiple(adaBoostClassifier, testingFold, classifierColumn);
                double percentage = ((double) correct) / numTestExamples;
                 kFoldAccuracies.add(percentage);
            }

            nTimeAccuracies.add(DataHelper.getAverage(kFoldAccuracies));
        }
        System.out.println("Average accuracy was " + DataHelper.getAverage(nTimeAccuracies));
        System.out.println("Standard Deviation was " + DataHelper.getStdev(nTimeAccuracies));
    }

    /**
     * Trains an ID3 AdaBoost classifier that will be used for classifying test data.
     * @param dataset The original dataset
     * @param classifierColumn The column being used for classification
     * @param attributes A list of attributes being used
     * @param numTrees The number of trees the forest should return
     * @return An ID3 AdaBoost classifier with weights and ID3 stumps.
     */
    private static AdaBoostClassifier trainAdaBoostClassifier(ArrayList<String[]> dataset, int classifierColumn, HashSet<Integer> attributes, int numTrees) {
        Node[] adaBoostClassifier = new Node[numTrees];
        double[] adaBoostWeights = new double[numTrees];
        double[] weightsOfData = new double[dataset.size()];

        for(int i = 0; i < numTrees; i++) {
            //Resample everywhere except the initial dataset
            if(i!=0){
                //dataset = resampleData(dataset, weightsOfData, i); Used for getting repeatable results with predictable seed
                dataset = resampleData(dataset, weightsOfData);
            }
            //Fills weight of the dataset to be 1/number of dataset size
            Arrays.fill(weightsOfData, 1.0/dataset.size());

            //Trains stump
            adaBoostClassifier[i] = trainStump(dataset, classifierColumn, attributes);

            //Figures out how many elements were classified incorrectly and keeps track of the sum of their weights
            double totalError = 0;
            HashSet<Integer> incorrectIndices = new HashSet<Integer>();
            for(int j = 0; j<dataset.size(); j++){
                String[] example = dataset.get(j);
                String modelClassification = ID3.classify(adaBoostClassifier[i], example);
                if(!modelClassification.equals(example[classifierColumn])){
                    incorrectIndices.add(j);
                    totalError += weightsOfData[j];
                }
            }

            //Small error term to totalError so it cannot be divided by 0
            //Log 0 also not possible under this this calculation
            double c = 0.0000000000000000001;
            double aT = 0.5*Math.log((1-totalError +c)/(totalError+c));
            double Zt = 0;

            //Finds new weights for examples based on if it was correctly classified.
            for(int j = 0; j<dataset.size(); j++){
                if(incorrectIndices.contains(j)){
                    weightsOfData[j] *= Math.exp(aT);
                }
                else{
                    weightsOfData[j] *= Math.exp(-aT);
                }
                Zt += weightsOfData[j];
            }

            //Normalizes weights by their weights by Zt, the cumulative sum of weights
            for(int j = 0; j<dataset.size(); j++){
                weightsOfData[j] = weightsOfData[j]/Zt;
            }
            adaBoostWeights[i] = aT;
        }


        return new AdaBoostClassifier(adaBoostClassifier, adaBoostWeights);
    }

    /**
     * Trains a stump that will be used in the AdaBoost classifier. First finds the attribute that best splits the current
     * dataset and then calls the train ID3 classifier to create a stump.
     * @param dataset The dataset being used for training the stump
     * @param classifierColumn The column where the classifier is
     * @param attributes The attributes that could be used for predictions
     * @return A stump that will be used for classification
     */
    private static Node trainStump(ArrayList<String[]> dataset, int classifierColumn, HashSet<Integer> attributes) {
        InformationGain maxGain = null;

        ArrayList<HashSet<String>> uniqueAttributeValues = ID3.preTrainID3(dataset, classifierColumn);
        HashMap<String, Integer> classifierValueOccurences = new HashMap<String, Integer>();
        for (int i = 0; i<dataset.size(); i++){
            String classname = dataset.get(i)[classifierColumn];
            if(!classifierValueOccurences.containsKey(classname)){
                classifierValueOccurences.put(classname, 1);
            }
            else{
                int classnameValue = classifierValueOccurences.get(classname);
                classifierValueOccurences.put(classname, classnameValue+1);
            }
        }

        //Finds the attribute which best separates the dataset using InformationGain
        for (int attribute : attributes) {
            double gain = ID3.getInformationGain(dataset, attribute, classifierValueOccurences, uniqueAttributeValues.get(attribute), classifierColumn);
            if (maxGain == null) {
                maxGain = new InformationGain(attribute, gain);
            }
            else if (maxGain.getValue() < gain) {
                maxGain = new InformationGain(attribute, gain);
            }
        }

        //Creates a HashSet containing the single attribute to be used for the ID3 classifier
        HashSet<Integer> attribute = new HashSet<>();
        attribute.add(maxGain.getAttribute());

        return ID3.trainID3Classifer(dataset, classifierColumn, attribute, uniqueAttributeValues);
    }

    /**
     * Resamples the dataset based on the distribution of weights. Used to get repeatable results.
     * @param dataset The dataset to be sampled
     * @param weightsOfData The weights of each item in the dataset
     * @param seed A number used to set the random seed
     * @return The new dataset that will be used
     */
    private static ArrayList<String[]> resampleData(ArrayList<String[]> dataset, double[] weightsOfData, int seed) {
        ArrayList<String[]> sampledDataset = new ArrayList<String[]>();
        Random random = new Random(seed);
        for(int i = 0; i<dataset.size(); i++){
            double randomNumber = random.nextDouble();
            boolean found = false;
            double sum = 0;
            for(int j = 0; j<weightsOfData.length &&!found; j++){
                sum += weightsOfData[j];
                if(randomNumber < sum){
                    found = true;
                    sampledDataset.add(dataset.get(j));
                }
            }
        }
        return sampledDataset;
    }

    /**
     * Resamples the dataset based on the distribution of weights.
     * @param dataset The dataset to be sampled
     * @param weightsOfData The weights of each item in the dataset
     * @return The new dataset that will be used
     */
    private static ArrayList<String[]> resampleData(ArrayList<String[]> dataset, double[] weightsOfData) {
        ArrayList<String[]> sampledDataset = new ArrayList<String[]>();
        Random random = new Random();
        for(int i = 0; i<dataset.size(); i++){
            double randomNumber = random.nextDouble();
            boolean found = false;
            double sum = 0;
            for(int j = 0; j<weightsOfData.length &&!found; j++){
                sum += weightsOfData[j];
                if(randomNumber < sum){
                    found = true;
                    sampledDataset.add(dataset.get(j));
                }
            }
        }
        return sampledDataset;
    }

    /**
     * Classifies a single example using the AdaBoost Classifier provided. This runs classification for each tree in the random forest
     * and returns the classification based on the most common class predicted by the trees in the forest.
     * @param adaBoostClassifier The AdaBoost classifier being used by this classification
     * @param exampleToClassify The example to be classified by the random forest
     * @return The most common class predicted by the trees in the random forest
     */
    private static String classify(AdaBoostClassifier adaBoostClassifier, String[] exampleToClassify) {
        //Create a hashmap to keep track of how the forest predicted classes.
        HashMap<String, Double> adaBoostClassifications = new HashMap<String, Double>();
        Node[] adaBoostTrees = adaBoostClassifier.getAdaBoostClassifier();
        double[] adaBoostWeights = adaBoostClassifier.getAdaBoostClassifierWeights();

        //For every decision tree in the classifier call ID3 classify since each tree is an ID3 tree.
        for(int i = 0; i< adaBoostTrees.length; i++){
            Node decisionTree = adaBoostTrees[i];
            String classification = ID3.classify(decisionTree, exampleToClassify);
            if(adaBoostClassifications.containsKey(classification)){
                double currentValue = adaBoostClassifications.get(classification);
                double weight = adaBoostWeights[i];
                adaBoostClassifications.put(classification, currentValue+weight);
            }
            else{
                double weight = adaBoostWeights[i];
                adaBoostClassifications.put(classification, weight);
            }
        }

        //Find the class that was predicted the most often by the AdaBoost classifier and return it
        String[] classesPredicted = adaBoostClassifications.keySet().toArray(new String[0]);
        String maxClassification = classesPredicted[0];
        for(int i = 1; i < classesPredicted.length; i++){
            if(adaBoostClassifications.get(maxClassification) < adaBoostClassifications.get(classesPredicted[i])){
                maxClassification = classesPredicted[i];
            }
        }
        return maxClassification;
    }

    /**
     * Classifies multiple examples by calling the classify function in AdaBoost.java.
     * If the predicted class is correct for an example, the algorithm increments `correct` which keeps track of correctly
     * classified examples.
     * @param adaBoostClassifier The adaBoost classifier being used by this classification
     * @param examplesToClassify The examples to be classified by the random forest
     * @param classifierColumn The column to be predicted
     * @return The number of correctly classified instances.
     */
    public static int classifyMultiple(AdaBoostClassifier adaBoostClassifier, String[][] examplesToClassify, int classifierColumn ){
        int correct = 0;
        for (int i = 0; i < examplesToClassify.length; i++) {
            String classification = classify(adaBoostClassifier, examplesToClassify[i]);
            if (classification.equals(examplesToClassify[i][classifierColumn])) {
                correct++;
            }
        }
        return correct;
    }

}

