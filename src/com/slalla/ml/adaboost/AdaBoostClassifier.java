package com.slalla.ml.adaboost;

import com.slalla.ml.id3.Node;

/**
 * A class used to represent an AdaBoostClassifier made of ID3 stumps
 */
public class AdaBoostClassifier {
    /**
     * The set of stumps being used for classification
     */
    private Node[] adaBoostClassifier;

    /**
     * The set of weights each stump plays a role in predictions
     */
    private double[] adaBoostClassifierWeights;

    public AdaBoostClassifier(Node[] adaBoostClassifier, double[] adaBoostClassifierWeights) {
        this.adaBoostClassifier = adaBoostClassifier;
        this.adaBoostClassifierWeights = adaBoostClassifierWeights;
    }

    public Node[] getAdaBoostClassifier() {
        return adaBoostClassifier;
    }

    public void setAdaBoostClassifier(Node[] adaBoostClassifier) {
        this.adaBoostClassifier = adaBoostClassifier;
    }

    public double[] getAdaBoostClassifierWeights() {
        return adaBoostClassifierWeights;
    }

    public void setAdaBoostClassifierWeights(double[] adaBoostClassifierWeights) {
        this.adaBoostClassifierWeights = adaBoostClassifierWeights;
    }
}
