package com.slalla.ml.dataTransformation;

import java.util.*;

public class DataHelper {

    public static ArrayList<String[][]> kfoldSplit(String[][] inputMatrix, int k) {
        //3 folds, dsetSize = 98
        //foldsize =  ⌈98/3⌉ = 33
        //fold 0 = 0, 32 (33)
        //fold 1 = 33, 65 (33)
        //fold 2 66, 98 (32)  Results in one larger fold
        ArrayList<String[][]> folds = new ArrayList<String[][]>();
        int foldSize = (int) (Math.ceil(inputMatrix.length / (k + 0.0)));
        for (int i = 0; i < k - 1; i++) {
            folds.add(Arrays.copyOfRange(inputMatrix, i * foldSize, (i + 1) * foldSize));
        }
        folds.add(Arrays.copyOfRange(inputMatrix, (k - 1) * foldSize, inputMatrix.length));
        return folds;
    }

    /**
     * Shuffles the data in a 2d String array and returns it. Used for getting predictable results
     * @param seed      The random number seed that you have selected
     * @param inputData The 2d array you wish to shuffle.
     * @return
     */
    public static String[][] shuffleData(int seed, String[][] inputData) {
        ArrayList<String[]> inputDataAsList = new ArrayList<String[]>();
        inputDataAsList.addAll(Arrays.asList(inputData));
        Random shuffleSeed = new Random(seed);
        Collections.shuffle(inputDataAsList, shuffleSeed);
        return inputDataAsList.toArray(new String[0][0]);
    }

    /**
     * * Shuffles the data in a 2d String array and returns it.
     * @param inputData The 2d array you wish to shuffle.
     * @return
     */
    public static String[][] shuffleData(String[][] inputData) {
        ArrayList<String[]> inputDataAsList = new ArrayList<String[]>();
        inputDataAsList.addAll(Arrays.asList(inputData));
        Random shuffleSeed = new Random();
        Collections.shuffle(inputDataAsList, shuffleSeed);
        return inputDataAsList.toArray(new String[0][0]);
    }


    public static double getStdev(ArrayList<Double> averages) {
        double sum = 0;
        double populationMean = getAverage(averages);

        for(Double averageI : averages){
            sum += Math.pow((averageI - populationMean),2);
        }

        return Math.sqrt(sum/averages.size());
    }

    public static double getAverage(ArrayList<Double> valuesToAverage) {
        double sum = 0;
        for (Double aDouble : valuesToAverage) {
            sum += aDouble;
        }
        return sum/valuesToAverage.size();
    }

    public static String[][] combineFolds(ArrayList<String[][]> folds, HashSet<Integer> foldsToUse) {
        ArrayList<String[]> arrayList = new ArrayList<String[]>();
        for (Integer foldToUse : foldsToUse) {
            String[][] fold = folds.get(foldToUse);
            arrayList.addAll(Arrays.asList(fold));
        }
        return arrayList.toArray(new String[0][0]);
    }
}
