package com.slalla.ml.dataTransformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class DataTransformer {
    private static boolean DEBUG = false;

    public static void main(String[] args) {

        //Select file to parse
        String[] fileNames = {"breast-cancer-wisconsin.data", "car.data", "ecoli.data", "letter-recognition.data", "mushroom.data"};
        Scanner inputScanner = new Scanner(System.in);

        int input = 99;
        do {
            System.out.println("Welcome to the Data Transformer. Please select a dataset to work with:");
            for (int i = 0; i < fileNames.length; i++) {
                System.out.println(i + 1 + ". " + fileNames[i]);
            }
            input = inputScanner.nextInt();
        } while (input < 1 || input > 5);

        String fileName = fileNames[input - 1];

        //Count number of rows and columns to make Matrix
        int[] rowsColumns;
        try {
            rowsColumns = FileHelper.getNumRowsColumns(fileName);
        } catch (FileNotFoundException e) {
            if (DEBUG) {
                e.printStackTrace();
            }
            System.out.println("Error reading number of rows and columns from file");
            return;
        }

        //Set up of data structures for reading in data
        int numRows = rowsColumns[0];
        int numColumns = rowsColumns[1];

        //The arrayList of indices we need to revisit because the input data may be incomplete
        //Stored as an integer array with a[0] = x a[1] = y for a pair (x,y)
        ArrayList<int[]> indicesToRevisit = new ArrayList<int[]>();

        //The arrayList of the most common values in the array.
        //This will be needed for replacement,
        String[] mostCommon;

        //The hashSet representing the unique values for each column
        HashSet[] uniqueAttributeValues = new HashSet[numColumns];
        for (int i = 0; i < numColumns; i++) {
            uniqueAttributeValues[i] = new HashSet<String>();
        }

        //Matrix of data
        String[][] inputMatrix;
        try {
            inputMatrix = readInValues(numRows, numColumns, indicesToRevisit, uniqueAttributeValues, fileName);
        } catch (FileNotFoundException e) {
            System.out.println("Error reading in file");
            return;
        }


        mostCommon = findMostCommon(inputMatrix);

        //Prints all the debugging information about unique values and indices to revisit
        if (DEBUG) {
            System.out.println(Arrays.toString(mostCommon));
            for (HashSet hashSet : uniqueAttributeValues) {
                System.out.println(hashSet.size() + ":" + hashSet);
            }

            for (int[] array : indicesToRevisit) {
                System.out.println(Arrays.toString(array));
            }
        }


        //Replace all "?" values with most common for that column
        for (int i = 0; i < indicesToRevisit.size(); i++) {
            int[] index = indicesToRevisit.get(i);
            inputMatrix[index[0]][index[1]] = mostCommon[index[1]];
            if (DEBUG) {
                System.out.println(Arrays.toString(indicesToRevisit.get(i)) + " was replaced with " + mostCommon[index[1]]);
            }
        }
        System.out.println(indicesToRevisit.size() + " attributes were filled since they were missing. \n");

        input = 99;
        HashSet<Integer> columnsToIgnore = new HashSet<Integer>();
        do {
            System.out.println("What transformations would you like to make?");
            System.out.println("1 to remove a column");
            System.out.println("2 to change a column to discrete values");
            System.out.println("-1 to finish");
            input = inputScanner.nextInt();

            if (input == 1) {
                System.out.println("What column would you like to remove?");
                int selectedColumn = inputScanner.nextInt();
                if (columnsToIgnore.contains(selectedColumn)) {
                    System.out.println("That column was already removed.\n");
                }
                else if (selectedColumn < 0 || selectedColumn > numColumns - 1) {
                    System.out.println("That column is not valid.\n");
                }
                else {
                    columnsToIgnore.add(selectedColumn);
                    System.out.println("Column " + selectedColumn + "was removed");
                }
            }
            else if (input == 2) {
                System.out.println("What column would you like to convert?");
                int selectedColumn = inputScanner.nextInt();
                if (columnsToIgnore.contains(selectedColumn)) {
                    System.out.println("That column was removed already.\n");
                }
                else if (selectedColumn < 0 || selectedColumn > numColumns - 1) {
                    System.out.println("That column is not valid.\n");
                }
                else {
                    System.out.println("How many buckets would you like to convert that variable to?");
                    int numBuckets = inputScanner.nextInt();
                    try {
                        continuousToDiscreteBySubRange(inputMatrix, selectedColumn, numBuckets);
                        System.out.println("Column " + selectedColumn + "was converted into " + numBuckets + " values.\n");
                    } catch (NumberFormatException e) {
                        System.out.println("Sorry that column cannot be converted.\n");
                    }
                }
            }
        } while (input != -1);

        String newFile = "";
        newFile = fileName.substring(0, fileName.indexOf(".data")) + "2.data";

        try {
            writeToFile(newFile, inputMatrix, columnsToIgnore);
        } catch (FileNotFoundException e) {
            System.out.println("Could not write to " + newFile);
        }
    }

    private static String[] findMostCommon(String[][] inputMatrix) {
        String[] mostCommon = new String[inputMatrix[0].length];
        for (int j = 0; j < inputMatrix[0].length; j++) {
            //The unique values per column
            HashSet<String> uniqueVals = new HashSet<String>();
            //The HashMap used to keep track of the number of times a specific value appears in a column
            HashMap<String, Integer> keyValues = new HashMap<String, Integer>();
            for (int i = 0; i < inputMatrix.length; i++) {
                String value = inputMatrix[i][j];
                if (value.equals("?")) {

                }
                else if (uniqueVals.add(value)) {
                    keyValues.put(value, 1);
                }
                else {
                    keyValues.put(value, keyValues.get(value) + 1);
                }
            }

            Set<String> keySet = keyValues.keySet();
            String[] keys = keySet.toArray(keySet.toArray(new String[0]));

            String maxKey = keys[0];
            int maxValueForColumn = keyValues.get(maxKey);

            for (int i = 1; i < keys.length; i++) {
                String key = keys[i];
                int keyValue = keyValues.get(key);

                if (keyValue > maxValueForColumn) {
                    maxKey = key;
                    maxValueForColumn = keyValue;
                }
            }
            mostCommon[j] = maxKey;
            System.out.println("Summary of values for column " + j);
            System.out.println(keyValues+"\n");
        }
        return mostCommon;
    }

    public static String[][] readInValues(int numRows, int numColumns, ArrayList<int[]> indicesToRevisit, HashSet[] uniqueAttributeValues, String fileName) throws FileNotFoundException {
        String[][] inputMatrix = new String[numRows][numColumns];

        //Create file and scanner to read in values
        File file = new File(fileName);
        Scanner sc = new Scanner(file);

        for (int i = 0; i < numRows; i++) {
            String currentLine = sc.nextLine();
            Scanner sc2 = new Scanner(currentLine);
            if (!fileName.equals("ecoli.data")) {
                sc2.useDelimiter(",");
            }
            for (int j = 0; j < numColumns; j++) {
                String value = sc2.next();
                inputMatrix[i][j] = value;

                if (value.equals("?")) {
                    //If the value is unknown we should add it to a list of indices we need to refill later
                    int[] indices = new int[2];
                    indices[0] = i;
                    indices[1] = j;
                    indicesToRevisit.add(indices);
                }
                else {
                    //otherwise we will try to add it to the hash set to see if it needs be added or not.
                    uniqueAttributeValues[j].add(value);
                }
            }
        }
        return inputMatrix;
    }

    /**
     * Writes the inputMatrix to a file.
     * @param newFile         The name of the new file
     * @param inputMatrix     The matrix to be output
     * @param columnsToIgnore The columns to be ignored
     * @throws FileNotFoundException
     */
    private static void writeToFile(String newFile, String[][] inputMatrix, HashSet<Integer> columnsToIgnore) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(newFile);

        for (int i = 0; i < inputMatrix.length; i++) {
            for (int j = 0; j < inputMatrix[0].length; j++) {
                boolean ignore = columnsToIgnore.contains(j);
                if (!ignore) {
                    if (j != inputMatrix[0].length - 1) {
                        pw.print(inputMatrix[i][j] + ",");
                    }
                    else {
                        pw.print(inputMatrix[i][j]);
                    }
                }
            }
            pw.println();
        }
        pw.close();
    }

    /**
     * Converts a column to a discrete column based on the column given and the number of buckets selected
     * @param array      The array we wish to change
     * @param column     The column we wish to convert
     * @param numBuckets The number of buckets we want to convert to
     */
    public static void continuousToDiscreteBySubRange(String[][] array, int column, int numBuckets) {
        double min = Double.parseDouble(array[0][column]);
        double max = Double.parseDouble(array[1][column]);
        if (min > max) {
            double temp = max;
            max = min;
            min = temp;
        }

        for (int i = 2; i < array.length; i++) {
            Double value = Double.parseDouble(array[i][column]);
            if (value > max) {
                max = value;
            }
            else if (value < min) {
                min = value;
            }
        }

        double rangeOfBucket = (max - min) / numBuckets;

        //Checks which range to assign to the cell
        for (int i = 0; i < array.length; i++) {
            Double value = Double.parseDouble(array[i][column]);
            int multiplier = 1;
            boolean added = false;
            while (multiplier < numBuckets && !added) {
                if (value < min + multiplier * rangeOfBucket) {
                    added = true;
                    array[i][column] = multiplier + "";
                }
                multiplier++;
            }
            //Double multiplication may cause some problems for the last bucket so this should be a catch all bucket
            if (!added) {
                array[i][column] = numBuckets + "";
            }
        }
    }
}
