package com.slalla.ml.dataTransformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class FileHelper {
    public static int[] getNumRowsColumns(String fileName) throws FileNotFoundException {
        int rowsColumns[] = new int[2];
        File file = new File(fileName);
        //Count number of rows and columns to make Matrix
        Scanner sc = new Scanner(file);
        int numColumns = 0;
        Scanner columnCounter = new Scanner(sc.nextLine());
        if (!fileName.equals("ecoli.data")) {
            columnCounter.useDelimiter(",");
        }
        while (columnCounter.hasNext()) {
            columnCounter.next();
            numColumns++;
        }

        int numRows = 1;
        while (sc.hasNextLine()) {
            sc.nextLine();
            numRows++;
        }

        rowsColumns[0] = numRows;
        rowsColumns[1] = numColumns;
        sc.close();
        return rowsColumns;
    }

    public static String[][] readInValues(int numRows, int numColumns, String fileName) throws FileNotFoundException {
        String[][] inputMatrix = new String[numRows][numColumns];

        //Create file and scanner to read in values
        File file = new File(fileName);
        Scanner sc = new Scanner(file);

        for (int i = 0; i < numRows; i++) {
            String currentLine = sc.nextLine();
            Scanner sc2 = new Scanner(currentLine);
            if (!fileName.equals("ecoli.data")) {
                sc2.useDelimiter(",");
            }
            for (int j = 0; j < numColumns; j++) {
                String value = sc2.next();
                inputMatrix[i][j] = value;
            }
        }
        return inputMatrix;
    }
}
