package com.slalla.ml.id3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class AttributeNode extends Node implements Serializable {

    private int column;

    public AttributeNode(int column){
        super();
        this.column = column;
    }

    public int getColumn(){
        return column;
    }

//    public void writeObject(ObjectOutputStream outputStream)throws IOException{
//        outputStream.defaultWriteObject();
//        for(Node child: getChildren()){
//            child.writeObject(outputStream);
//        }
//    }
//
//    public void readObject(ObjectInputStream inputStream) throws ClassNotFoundException, IOException {
//        inputStream.defaultReadObject();
//
//    }
}
