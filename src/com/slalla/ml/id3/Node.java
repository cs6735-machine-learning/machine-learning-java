package com.slalla.ml.id3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public abstract class Node implements Serializable{

    private ArrayList<Node> children;

    public Node(){
        this.children = new ArrayList<Node>();
    }

    public ArrayList<Node> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Node> children) {
        this.children = children;
    }

//    public void writeObject(ObjectOutputStream outputStream){
//
//    }
}
