package com.slalla.ml.id3;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class ValueNode extends Node implements Serializable {

    private String value;

    public ValueNode(String value){
        super();
        this.value = value;
    }

    public String getValue() {
        return value;
    }

//    public void writeObject(ObjectOutputStream outputStream)throws IOException {
//        outputStream.defaultWriteObject();
//
//    }
//
//    public void readObject(ObjectInputStream inputStream) throws ClassNotFoundException, IOException {
//        inputStream.defaultReadObject();
//
//    }
}
