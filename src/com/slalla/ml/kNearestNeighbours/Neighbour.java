package com.slalla.ml.kNearestNeighbours;

public class Neighbour {
    private double distance;
    private String className;

    public Neighbour(double distance, String className) {
        this.distance = distance;
        this.className = className;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
