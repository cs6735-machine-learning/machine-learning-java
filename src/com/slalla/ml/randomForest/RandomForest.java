package com.slalla.ml.randomForest;

import com.slalla.ml.dataTransformation.DataHelper;
import com.slalla.ml.dataTransformation.FileHelper;
import com.slalla.ml.id3.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;

public class RandomForest {

    public static void main(String[] args) throws IOException {
        //Select file to parse
        String[] fileNames = {"breast-cancer-wisconsin2.data", "car.data", "ecoli2.data", "letter-recognition.data", "mushroom2.data"};
        Scanner inputScanner = new Scanner(System.in);

        int input = 99;
        do {
            System.out.println("Welcome to the Random Forest Classifier. Please select a dataset to work with:");
            for (int i = 0; i < fileNames.length; i++) {
                System.out.println(i + 1 + ". " + fileNames[i]);
            }
            input = inputScanner.nextInt();
        } while (input < 1 || input > 5);

        String fileName = fileNames[input - 1];

        int[] rowCols;
        try {
            rowCols = FileHelper.getNumRowsColumns(fileName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        int numRows = rowCols[0];
        int numCols = rowCols[1];


        String[][] inputMatrix;

        try {
            inputMatrix = FileHelper.readInValues(numRows, numCols, fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }


        int classifierColumn = numCols;
        do {
            System.out.println("Please select the column you wish to predict. There are " + numCols + " columns.");
            classifierColumn = inputScanner.nextInt();
        } while (classifierColumn < 0 || classifierColumn >= numCols);

        int m = numCols;
        do {
            System.out.println("Please select the number of attributes you wish to use in the randomForest algorithm. There are " + (numCols-1) + " attributes.");
            m = inputScanner.nextInt();
        } while (m < 0 || m > numCols-1);

        int numTrees = 0;
        do {
            System.out.println("Please select the number of trees you wish to use in the randomForest algorithm. 20 is typically used.");
            numTrees = inputScanner.nextInt();
        } while (numTrees <= 0);


        //Number of times to run k-fold-cross validation
        ArrayList<Double> nTimeAccuracies = new ArrayList<>();
        for (int ntime = 0; ntime < 10; ntime++) {
            //Shuffle
            //inputMatrix = DataHelper.shuffleData(ntime, inputMatrix); used for predictable shuffled data
            inputMatrix = DataHelper.shuffleData(inputMatrix);

            int numberOfFolds = 5;
            ArrayList<String[][]> folds = DataHelper.kfoldSplit(inputMatrix, numberOfFolds);
            ArrayList<Double> kFoldAccuracies = new ArrayList<Double>();
            for (int i = 0; i < folds.size(); i++) {
                HashSet<Integer> foldsToUse = new HashSet<Integer>();
                int fold = i;
                while (foldsToUse.size() != numberOfFolds - 1) {
                    fold = (fold + 1) % (numberOfFolds);
                    foldsToUse.add(fold);
                }
                System.out.println(foldsToUse);
                String[][] combinedFold = DataHelper.combineFolds(folds, foldsToUse);
                String[][] testingFold = folds.get((fold + 1) % (numberOfFolds));


                HashSet<Integer> attributes = new HashSet<Integer>();
                for(int j = 0; j< numCols; j++){
                    if(j != classifierColumn){
                        attributes.add(j);
                    }
                }

                ArrayList<String[]> inputMatrixConvert = new ArrayList<String[]>(Arrays.asList(combinedFold));
                Node[] randomForest = trainRandomForestClassifier(inputMatrixConvert, classifierColumn, attributes, m, numTrees);

                if(i==1) {
                    FileOutputStream fos = new FileOutputStream("RandomForest.txt");
                    ObjectOutputStream outFile = new ObjectOutputStream(fos);

                    for (int k = 0; k < numTrees; k++) {  //writes the binary object file
                        outFile.writeObject(randomForest[k]);
                    }

                    outFile.close();  //closes the binary io file.
                }

                int numTestExamples = testingFold.length;

                int correct = classifyMultiple(randomForest, testingFold, classifierColumn);
                double percentage = ((double) correct) / numTestExamples;
                kFoldAccuracies.add(percentage);
            }
            nTimeAccuracies.add(DataHelper.getAverage(kFoldAccuracies));
        }
        System.out.println("Average accuracy was " + DataHelper.getAverage(nTimeAccuracies));
        System.out.println("Standard Deviation was " + DataHelper.getStdev(nTimeAccuracies));
    }

    /**
     * Randomly chooses N examples from datasetToSample, a matrix of size N, with replacement. Used for testing to get replicatable results
     * @param datasetToSample The dataset being sampled.
     * @param seed The random seed we will use for generating random numbers.
     * @return New matrix of size N with entries randomly chosen from datasetToSample.
     */
    private static ArrayList<String[]> randomlySampleDataset(ArrayList<String[]> datasetToSample, int seed) {
        Random sampleSeed = new Random(seed);
        ArrayList<String[]> randomSamples = new ArrayList<String[]>();
        while (randomSamples.size()<datasetToSample.size()){
            int randomNum = sampleSeed.nextInt(datasetToSample.size());
            randomSamples.add(datasetToSample.get(randomNum));
        }
        return randomSamples;
    }

    /**
     * Randomly chooses N examples from datasetToSample, a matrix of size N, with replacement.
     * @param datasetToSample The dataset being sampled.
     * @return New matrix of size N with entries randomly chosen from datasetToSample.
     */
    private static ArrayList<String[]> randomlySampleDataset(ArrayList<String[]> datasetToSample) {
        Random sampleSeed = new Random();
        ArrayList<String[]> randomSamples = new ArrayList<String[]>();
        while (randomSamples.size()<datasetToSample.size()){
            int randomNum = sampleSeed.nextInt(datasetToSample.size());
            randomSamples.add(datasetToSample.get(randomNum));
        }
        return randomSamples;
    }

    /**
     * Trains a random forest based by using the ID3 algorithm.
     * @param dataset The original dataset being used
     * @param classifierColumn The classification column
     * @param attributes The hashset of attributes in the original dataset
     * @param m The number of attributes to use per tree
     * @param numTrees The number of trees to create in the forest
     * @return A Node array which will represent a random forest
     */
    private static Node[] trainRandomForestClassifier(ArrayList<String[]> dataset, int classifierColumn, HashSet<Integer> attributes, int m, int numTrees) {
        Node[] randomForest = new Node[numTrees];
        Integer[] attributesArray = attributes.toArray(new Integer[0]);
        for(int i = 0; i < numTrees; i++){
            ArrayList<String[]> sampleDataset = randomlySampleDataset(dataset, i); //used for testing to get predictable results
//            ArrayList<String[]> sampleDataset = randomlySampleDataset(dataset);
            HashSet<Integer> mAttributes = new HashSet<Integer>();

            Random seed = new Random(i); //used for testing to get predictable results
//            Random seed = new Random();
            while(mAttributes.size() < m){
                mAttributes.add(attributesArray[seed.nextInt(attributesArray.length)]);
            }
            ArrayList<HashSet<String>> uniqueAttributes = ID3.preTrainID3(sampleDataset, classifierColumn);
            randomForest[i] = ID3.trainID3Classifer(sampleDataset, classifierColumn, mAttributes, uniqueAttributes);
        }
        return randomForest;
    }

    /**
     * Classifies a single example using the randomForest provided. This runs classification for each tree in the random forest
     * and returns the classification based on the most common class predicted by the trees in the forest.
     * @param randomForest The random forest classifier being used by this classification
     * @param exampleToClassify The example to be classified by the random forest
     * @return The most common class predicted by the trees in the random forest
     */
    private static String classify(Node[] randomForest, String[] exampleToClassify) {
        //Create a hashmap to keep track of how the forest predicted classes.
        HashMap<String, Integer> randomForestClassifications = new HashMap<String, Integer>();

        //For every decision tree in the forest call ID3 classify since each tree is an ID3 tree.
        for(Node decisionTree : randomForest){
            String classification = ID3.classify(decisionTree, exampleToClassify);
            if(randomForestClassifications.containsKey(classification)){
                int currentValue = randomForestClassifications.get(classification);
                randomForestClassifications.put(classification, currentValue+1);
            }
            else{
                randomForestClassifications.put(classification, 1);
            }
        }

        //Find the class that was predicted the most often by the random forest and return it
        String[] classesPredicted = randomForestClassifications.keySet().toArray(new String[0]);
        String maxClassification = classesPredicted[0];
        for(int i = 1; i < classesPredicted.length; i++){
            if(randomForestClassifications.get(maxClassification) < randomForestClassifications.get(classesPredicted[i])){
                maxClassification = classesPredicted[i];
            }
        }
        return maxClassification;
    }

    /**
     * Classifies multiple examples by calling the classify function in RandomForest.java.
     * If the predicted class is correct for an example, the algorithm increments `correct` which keeps track of correctly
     * classified examples.
     * @param randomForest The random forest classifier being used by this classification
     * @param examplesToClassify The examples to be classified by the random forest
     * @param classifierColumn The column to be predicted
     * @return The number of correctly classified instances.
     */
    public static int classifyMultiple(Node[] randomForest, String[][] examplesToClassify, int classifierColumn ){
        int correct = 0;
        for (int i = 0; i < examplesToClassify.length; i++) {
            String classification = classify(randomForest, examplesToClassify[i]);
            if (classification.equals(examplesToClassify[i][classifierColumn])) {
                correct++;
            }
        }
        return correct;
    }

}
